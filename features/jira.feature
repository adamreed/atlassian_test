Feature: JIRA interaction
  In order to notify the team about a defect
  As a tester
  I need to record the details in JIRA

  Background:
    Given I am on the JIRA landing page
    And I log into JIRA as "reed.adam+AT@gmail.com"

    @create
    Scenario: Create a bug
      When I click the Create link
      And I create a bug with default content
      Then I expect the success message modal to confirm success

    @search
    Scenario: Search for a bug and verify the result
      When I search JIRA for "Test Summary Title"
      Then I expect a relevant bug to be displayed in the search results

    @update
    Scenario: Update a bug found in the seach results
      When I search JIRA for "Test Summary Title"
      And I select the first bug
      And I update the bug content
      And I select the first bug
      Then I expect the bug to display the new content

