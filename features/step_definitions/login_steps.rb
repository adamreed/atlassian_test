When(/^I log into JIRA as "([^"]*)"$/) do |user|
  page = LandingPage.new(@browser)
  page.login_link.click

  page = LoginPage.new(@browser)
  page.login(user, 'test4567')
end