Given(/^I am on the (.*) page$/) do |page_name|
  @browser.navigate.to (path_to(page_name))
end

Given(/^I debug$/) do
  require 'pry'
  binding.pry
end