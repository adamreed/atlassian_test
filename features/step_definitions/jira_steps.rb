And(/^I click the Create link$/) do
  @page = SummaryPage.new(@browser)
  @page.create_link.click
end

And(/^I create a bug with default content$/) do
  @page = SummaryPage.new(@browser)
  @page.create_bug
end

Then(/^I expect the success message modal to confirm success$/) do
  @page = SummaryPage.new(@browser)
  wait = Selenium::WebDriver::Wait.new(:timeout => 60)
  wait.until { @page.modal_success_message.displayed? }
  expect(@page.modal_success_message.text).to include("success")
end

When(/^I search JIRA for "(.*)"$/) do |term|
  @page = SummaryPage.new(@browser)
  @page.search_for(term)
  @term = term

  @page = SearchResultsPage.new(@browser)
  wait = Selenium::WebDriver::Wait.new(:timeout => 20)
  wait.until { @page.first_search_result_id == @page.bug_id }
end

Then(/^I expect a relevant bug to be displayed in the search results$/) do
  @page = SearchResultsPage.new(@browser)
  #Any results are generated
  expect(@page.all_results.length).to be > 0

  #Relevant results are generated
  expect(@page.first_search_result.text).to include(@term)
end

When(/^I select the first bug$/) do
  @page = SearchResultsPage.new(@browser)

  # If the first bug is already open, don't click.
  if @page.first_search_result_id == @page.bug_id
    puts "\tBug #{@page.bug_id} has already been displayed by default."
  else
    @page.first_search_result.click
    wait = Selenium::WebDriver::Wait.new(:timeout => 60)
    wait.until { @page.first_search_result_id == @page.bug_id }
  end
end

And(/^I update the bug content$/) do
  @summary = "I Updated This Bug!"
  @description = "1. Open IE 6.0\n2. Do Anything"

  @page = SearchResultsPage.new(@browser)
  wait = Selenium::WebDriver::Wait.new(:timeout => 10)
  wait.until { @page.edit_link.displayed? }

  @bug_id = @page.bug_id
  @page.edit_link.click
  @page.update_bug({:summary => @summary,
                    :description => @description})

  wait.until { @page.modal_success_message.displayed? }
  expect(@page.modal_success_message.text).to include("has been updated.")
end

Then(/^I expect the bug to display the new content$/) do
  @page = SearchResultsPage.new(@browser)
  #no objects are reliable to be tracked/waited for here, so a sleep statement is required
  sleep 1
  #verify this is the same bug that we edited
  expect(@page.bug_id).to match(@bug_id)
  #verify the content matches what the tester supplied
  expect(@page.bug_title).to match(@summary)
  expect(@page.bug_description).to include(@description)
end