class SearchResultsPage
  include CreateBugModal
  include EditBugModal
  include Header
  include ModalMessage

  def initialize(browser)
    @browser = browser
  end

  # SEARCH RESULT LIST ELEMENTS
  def results_list
    @browser.find_element(:class, 'list-panel')
  end

  def all_results
    results_list.find_elements(:tag_name, 'li')
  end

  def first_search_result
    @browser.find_element(:class, 'splitview-issue-link')
  end

  def first_search_result_title
    first_search_result.text.split("\n")[1]
  end

  def first_search_result_id
    first_search_result.text.split("\n")[0].gsub(" ", "")
  end

  def open_bug(term)
    #returns the first item that matches
    @browser.find_element(:link_text, term).click
  end

  # MAIN BUG ELEMENTS
  def bug_id
    @browser.find_element(:id, 'key-val').text
  end

  def bug_title
    @browser.find_element(:id, 'summary-val').text
  end

  def edit_link
    @browser.find_element(:link_text, 'Edit')
  end

  def bug_description
    @browser.find_element(:class, 'user-content-block').text
  end
end

