class LoginPage
  def initialize(browser)
    @browser = browser
  end

  def username
    @browser.find_element(:id, 'username')
  end

  def password
    @browser.find_element(:id, 'password')
  end

  def submit_button
    @browser.find_element(:id, 'login-submit')
  end

  def login(user, pass)
    username.send_keys(user)
    password.send_keys(pass)
    submit_button.click
  end

end

