class LandingPage
  def initialize(browser)
    @browser = browser
  end

  def login_link
    @browser.find_element(:link_text, "Log In")
  end
end

