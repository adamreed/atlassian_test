def path_to(page_name)
  case page_name
    when /JIRA landing/i
      "https://jira.atlassian.com/browse/TST"
    else
      raise "Can't find mapping from \"#{page_name}\" to a path."
  end
end