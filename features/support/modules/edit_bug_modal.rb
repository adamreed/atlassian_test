module EditBugModal

  def modal_div
    @browser.find_element(:id, 'edit-issue-dialog')
  end

  def description_field
    @browser.find_element(:id, 'description')
  end

  def summary_field
    @browser.find_element(:id, 'summary')
  end

  def update_button
    @browser.find_element(:id, 'edit-issue-submit')
  end

  def loading_throbber
    @browser.find_element(:class, 'loading')
  end

  def update_bug(opts = {})
    opts[:summary] ||= "Updated Title"
    opts[:description] ||= "1. Step 1\n2. Step 2\n\ta. Sub-Step 2a."


    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    wait.until { summary_field.displayed? }

    summary_field.clear
    summary_field.send_keys opts[:summary]
    description_field.clear
    description_field.send_keys opts[:description]
    update_button.click

    wait = Selenium::WebDriver::Wait.new(:timeout => 20)
    wait.until { modal_success_message.displayed? }
  end

end