module CreateBugModal

  def modal_div
    @browser.find_element(:id, 'create-issue-dialog')
  end

  def issuetype_field
    @browser.find_element(:id, 'issuetype-field')
  end

  def summary_field
    @browser.find_element(:id, 'summary')
  end

  def create_button
    @browser.find_element(:id, 'create-issue-submit')
  end

  def loading_throbber
    @browser.find_element(:class, 'loading')
  end

  def issuetype_field_select(type="Bug")
    issuetype_field.click
    selection = @browser.find_element(:link_text, type)
    selection.click

    wait = Selenium::WebDriver::Wait.new(:timeout => 15)
    wait.until { issuetype_field.attribute("disabled") == nil }
  end

  def create_bug(opts = {})
    opts[:summary] ||= "Test Summary Title"
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    wait.until { issuetype_field.displayed? }

    issuetype_field_select
    summary_field.send_keys opts[:summary]
    create_button.click

    #wait = Selenium::WebDriver::Wait.new(:timeout => 40)
    #wait.until { !loading_throbber.displayed? }
  end

end