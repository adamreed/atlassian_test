module Header

  def create_link
    @browser.find_element(:id, 'create_link')
  end

  def search_field
    @browser.find_element(:id, 'quickSearchInput')
  end

  def search_for(term)
    search_field.send_keys(term)
    search_field.send_keys :return
  end

end

