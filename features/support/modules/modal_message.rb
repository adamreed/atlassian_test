module ModalMessage

  def modal_success_message
    @browser.find_element(:class, 'aui-message-success')
  end

end