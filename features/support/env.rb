$stdout.sync = true
require 'rubygems'
require 'bundler/setup'
require 'selenium-webdriver'

Before do |scenario|
  profile = Selenium::WebDriver::Firefox::Profile.from_name "default"
  profile.native_events = false

  @browser = Selenium::WebDriver.for :firefox, :profile => profile

  #p "Starting scenario: #{scenario.title}"
end

After do |scenario|
  if scenario.failed?

  end

  @browser.close
  #p "Ending scenario: #{scenario.title}"
end