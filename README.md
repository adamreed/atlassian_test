Atlassian_Test by Adam Reed
===========================
Cucumber/Selenium-WebDriver test suite for JIRA instances

Purpose
=======
This project contains tests which verify the basic functionality of creating, searching for and updating/editing bugs.

Installation
============
OSX:
-------
1. Clone the git repository
2. Make sure you have Ruby/rvm installed (ruby-2.1.1)
3. If you do not have bundler installed, run 'gem install bundler' from Terminal
4. From the project root (where the Gemfile is located) run 'bundle install'
5. If Firefox is not installed, install the latest selenium-compatible version here: https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/33.1.1/


Windows:
-------------
1. Clone the git repository
2. Install Ruby-2.0.0p598 (most stable option)
3. Install the RubyDevKit.
4. Install the following file to %RubyHome%/lib/ruby/2.0.0/rubygems/ssl-certs to resolve a known Rubygems issue.
   https://raw.githubusercontent.com/rubygems/rubygems/master/lib/rubygems/ssl_certs/AddTrustExternalCARoot-2048.pem
5. Install bundler: 'gem install bundler'
6. From the project root (where the Gemfile is located) run 'bundle install'
7. If Firefox is not installed, install the latest selenium-compatible version here: https://ftp.mozilla.org/pub/mozilla.org/firefox/releases/33.1.1/

Execution
=========
Choose a rake task to run, and run 'rake [taskname]' from Terminal

To run the suite, run 'rake jira' from the 'atlassian test' directory.


Assumptions Made
================
1. The user account is assumed to have create & edit privileges in JIRA
2. The user can have simultaneous sessions (assuming script is executed in parallel)
3. JIRA already contains data that matches the test spec when running 'search' or 'update' tasks independently. To be safe, run 'rake jira', so that the 'create' task can create the necessary data and fulfill the test dependency.


Test Expectations
==============
1. The "Create Bug" success condition requires that the modal display message appears and contains the word "success".
2. The "Search" success condition requires that greater than '0' results are listed, and that the first result contains the search phrase.
3. The "Update Bug" success condition requires that the final Bug ID matches the Bug that was edited, and the Bug Summary and Bug Description match supplied values.


Known Issues
============
1. This suite uses the minimum required fields, and is not intended to test the entire Bug dialog or fields. Errors with additional features
will not be caught.
2. Localization/translation is not supported at this time, so test expectations are defined explicitly in the code.
3. The test account password is stored in plain text and hard-coded, which is not awesome, but serves our current purpose.
4. A Selenium stale element error has appeared twice at line 29 of search_results_page.rb, but cannot be reproduced. So there's that. If
you're reading this comment, that means I never saw it again and am hoping for the best.